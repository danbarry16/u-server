FLAGS = -std=c99 -Os -s

all : userver

userver : main.c userver.h
	cc $(FLAGS) -o userver main.c

clean :
	-rm userver
