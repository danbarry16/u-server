#include <arpa/inet.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>

/**
 * userver.h A very simple single-file HTTP web server.
 * @author B[]
 **/
#ifndef USERVER_LIB
  #define USERVER_LIB 1.50
  #define USERVER_SEND(f, s) (send(f, s, userver_sfind(s, '\0') - s, MSG_NOSIGNAL))
  #ifndef USERVER_CUSTOM
    #define USERVER_HEADER_LINE "\r\n"
    #define USERVER_HEADER_BEG ("HTTP/1.1 200 OK" USERVER_HEADER_LINE "Content-Type: ")
  #endif

  struct Response{ long hash; char* mime; char* payload; int len; };
  char* www;
  int readMax;
  int fileMax;
  int cacheSize;
  char* buff;
  struct Response* wwwCache;
  int socketfd;
  fd_set active_fd_set;

  /**
   * userver_init() Initialise the server.
   * @param port The port number to be used.
   * @param dir The directory to be served.
   * @param read The maximum read length from a client.
   * @param file The maximum size for a file to be loaded.
   * @param cache The size of the cache to be used.
   * @return 0 if success, less than 0 if there is an error.
   **/
  int userver_init(int port, char* dir, int read, int file, int cache){
    www = dir;
    readMax = read;
    buff = (char*)malloc(readMax + 2);
    fileMax = file;
    cacheSize = cache;
    wwwCache = malloc(cacheSize * sizeof(struct Response));
    /* Create the socket */
    socketfd = socket(PF_INET, SOCK_STREAM, 0);
    struct sockaddr_in name =
      {.sin_family = AF_INET, .sin_port = htons(port), .sin_addr.s_addr = htonl(INADDR_ANY)};
    /* Bind the socket */
    if(socketfd >= 0 && bind(socketfd, (struct sockaddr*)&name, sizeof(name)) < 0) socketfd = -1;
    /* Check whether we initialized correctly */
    if(socketfd >= 0 && listen(socketfd, 1) < 0) socketfd = -1;
    /* Initialise set of active sockets */
    FD_ZERO(&active_fd_set);
    FD_SET(socketfd, &active_fd_set);
    return socketfd < 0 ? -1 : 0;
  }

  /**
   * userver_sfind() Find a character in a NULL terminated string.
   * @param a The string to be searched.
   * @param c The character to be located.
   * @return Pointer to the character, otherwise the end of the string.
   **/
  char* userver_sfind(char* a, char c){
    while(*a != '\0' && *a != c) ++a;
    return a;
  }

  /**
   * userver_scmp() Compare two NULL terminated strings, checking if equal.
   * @param a The first string to be checked.
   * @param b The substring to be checked that is NULL terminated.
   * @param c Treat as a NULL character in string a.
   * @return A pointer to the end of the match if equal, otherwise NULL.
   **/
  char* userver_scmp(char* a, char* b, char c){
    while(*a != '\0' && *b != '\0' && *a != c && *b != c){
      if(*a++ != *b++) return NULL;
    }
    return *a != *b && !(*a == c && *b == '\0') ? NULL : a;
  }

  /**
   * userver_process() Process a user's request and return relevant data.
   * @param req The request type.
   * @param path The unparsed path from the client.
   * @param raw The raw header options.
   * @return The data to be loaded, later freed if userver_loadfile() used.
   **/
  struct Response userver_process(char* req, char* path, char* raw);

  /**
   * userver_parselocation() Get the location part of a given URL.
   * @param path The URL to be parsed.
   * @return A pointer to the NULL terminated data, otherwise NULL.
   **/
  char* userver_parselocation(char* path){
    *(userver_sfind(path, '?')) = '\0';
    return path;
  }

  /**
   * userver_parseparams() Get a parameter for a URL, given a variable name.
   * @param path The URL to be parsed.
   * @param raw The pointer to the raw headers.
   * @param name The name of the variable to be located.
   * @return A pointer to the NULL terminated data, otherwise NULL.
   **/
  char* userver_parseparams(char* path, char* raw, char* name){
    path = userver_sfind(userver_parselocation(path), '&') + 1;
    char* param;
    while((param = userver_sfind(path, '&')) + 1 < raw){
      *param = '\0';
      path = userver_scmp(path, name, '=');
      if(path != NULL){
        return path + 1;
      }
      path = param + 1;
    }
    return NULL;
  }

  /**
   * userver_parseheader() Get parameter HTTP header attribute, given a name.
   * @param raw The raw HTTP header to be parsed.
   * @param name The name of the variable to be located.
   * @return A pointer to the NULL terminated data, otherwise NULL.
   **/
  char* userver_parseheader(char* raw, char* name){
    raw = userver_sfind(raw, '\n') + 1;
    char* param;
    while((param = userver_sfind(raw, '\n'))[1] != '\0'){
      *param = '\0';
      raw = userver_scmp(raw, name, ':');
      if(raw != NULL){
        return raw + 2;
      }
      raw = param + 1;
    }
    return raw[0] != '\0' && userver_scmp(name, "form", '\0') != NULL ? raw : NULL;
  }

  /**
   * userver_loadfile() Load file from disk from WWW directory.
   * @param filename The file to be served from a given directory.
   * @return The loaded file data. This will be freed by userver_process().
   **/
  struct Response* userver_loadfile(char* filename){
    int nameLen = userver_sfind(filename, '\0') - filename;
    /* Make sure a directory is set and name does not have path character */
    if(www != NULL && (userver_sfind(filename, '/') - filename) == nameLen){
      int wwwLen = userver_sfind(www, '\0') - www;
      char* location = malloc((wwwLen + nameLen + 1) * sizeof(char));
      for(int x = 0; x < wwwLen + nameLen + 1; x++){
        location[x] = x < wwwLen ? www[x] : filename[x - wwwLen];
      }
      struct stat filestat;
      if(stat(location, &filestat) == 0){
        long hash = filestat.st_atime ^ filestat.st_mtime ^ filestat.st_ctime;
        int i = hash % cacheSize;
        if(wwwCache[i].payload == NULL || wwwCache[i].hash != hash){
          if(wwwCache[i].payload != NULL){
            free(wwwCache[i].payload);
            wwwCache[i].payload = NULL;
          }
          FILE* f = fopen(location, "r");
          wwwCache[i].hash = hash;
          wwwCache[i].payload = malloc((fileMax + 1) * sizeof(char));
          wwwCache[i].len = fread(wwwCache[i].payload, sizeof(char), fileMax, f);
          fclose(f);
          free(location);
          wwwCache[i].payload[wwwCache[i].len] = '\0';
        }
        return &wwwCache[i];
      }
    }
    return NULL;
  }

  /**
   * userver_service() Process a client(s) waiting to be serviced.
   * @return 0 if clients serviced successfully, less than 0 on error, greater
   * than 0 to indicate a non-error state.
   **/
  int userver_service(){
    struct sockaddr_in clientName;
    socklen_t size = sizeof(clientName);
    if(socketfd < 0){
      return -1;
    }
    /* Block until input arrives on one or more active sockets */
    fd_set read_fd_set = active_fd_set;
    if(select(FD_SETSIZE, &read_fd_set, NULL, NULL, NULL) < 0){
      return -1;
    }
    /* Service all the sockets with input pending */
    int response = 0;
    for(int i = 0; i < FD_SETSIZE; ++i){
      if(FD_ISSET(i, &read_fd_set)){
        if(i == socketfd){
          /* Connection request on original socket */
          int fd = accept(socketfd, (struct sockaddr*)&clientName, &size);
          if(fd < 0){
            return -1;
          }
          struct timeval tv = {.tv_sec = 1, .tv_usec = 0};
          setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));
          response = 1;
          /* Read and process user request */
          int n, nTot = 0;
          while((n = read(fd, buff + nTot, readMax - nTot)) > 0){
            nTot += n;
            setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));
          }
          buff[nTot] = '\0';
          buff[nTot + 1] = '\0';
          char* loc = userver_sfind(buff, ' ');
          *loc = '\0';
          char* mtd = userver_sfind(loc + 2, ' ');
          *mtd = '\0';
          char* raw = userver_sfind(mtd + 2, '\n');
          *raw = '\0';
          /* Create and send response */
          struct Response r = userver_process(buff, loc + 1, raw + 1);
          if(r.mime != NULL && r.payload != NULL){
            USERVER_SEND(fd, USERVER_HEADER_BEG);
            USERVER_SEND(fd, r.mime);
            USERVER_SEND(fd, USERVER_HEADER_LINE);
            send(fd, r.payload, r.len >= 0 ? r.len : userver_sfind(r.payload, '\0') - r.payload, MSG_NOSIGNAL);
          }
          /* Finally, flush and disconnect */
          shutdown(fd, SHUT_WR);
          FD_CLR(fd, &active_fd_set);
        }
      }
    }
    return response;
  }

  /**
   * userver_loop() Service clients in an infinite loop and sleep.
   * @param sleep Time to sleep when no clients are detected in microseconds.
   **/
  void userver_loop(int sleep){
    struct timeval tv = {.tv_sec = 0, .tv_usec = sleep};
    for(;;) if(userver_service() == 0) select(0, NULL, NULL, NULL, &tv);
  }
#endif
