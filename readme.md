# uServer

uServer is an ultra lightweight Unix server implementation that weighs in under
256 lines (including comments) in a single header file.

Features:

* *Fast* - It spends minimal time processing clients and runs on a
single-thread (which means you can make guarantees about resource usage).
* *Small* - It is under 256 lines and uses minimal standard/Unix libraries. As
a result it compiles at ~15kB.
* *Load static files* - Loads small files from disk from a single directory
(user responsible for providing correct MIME types). Files are cached in RAM
and reloaded when a timestamp change is detected (this is not foolproof).
* *Parse variables* - Parses variables from the path URL (without decoding) and
HTTP header.
* *Dumb* - It doesn't do any magic.

The style is based on the [Unix
philosophy](https://en.wikipedia.org/wiki/Unix_philosophy), particularly:

> 1. Make each program do one thing well. To do a new job, build afresh rather
> than complicate old programs by adding new "features".

## Building

```
make clean # Remove any existing binary
make       # Create new binaries
```

## Running

To run the demo, simply run:

```
./userver -h
```

This will show the available options for running the server.

With the default settings, you can navigate to `http://127.0.0.1:9999/` and
view a test page example.

## Coding

See the [main.c](main.c) file for an example implementation. The [uServer
source](userver.h) is also well commented and easy to understand.
