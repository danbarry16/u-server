#include "userver.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * main.c
 *
 * The following is an example implementation of the userver project. It can be
 * built using the Makefile and tested at http://127.0.0.1:9999/ .
 **/

struct Response userver_process(char* req, char* path, char* raw){
  struct Response resp =
    {.mime = "text/html" USERVER_HEADER_LINE, .payload = "<h1>Error</h1>", .len = -1};
  if(strcmp(req, "GET") == 0){
    char* loc = userver_parselocation(path);
    /* If we see ?hello=world - reply! */
    char* param = userver_parseparams(path, raw, "hello");
    if(param != NULL && strcmp(param, "world") == 0){
      resp.payload = "<h1>Hello World!</h1>";
      return resp;
    }
    /* If we see /user, attempt to show the user their user agent */
    if(strcmp(loc, "/user") == 0){
      char* header = userver_parseheader(raw, "User-Agent");
      if(header != NULL){
        resp.mime = "text/plain" USERVER_HEADER_LINE;
        resp.payload = header;
      }else{
        resp.payload = "<h1>Cannot not find user agent</h1>";
      }
      return resp;
    }
    if(strcmp(loc, "/")           == 0 ||
       strcmp(loc, "/index")      == 0 ||
       strcmp(loc, "/index.htm")  == 0 ||
       strcmp(loc, "/index.html") == 0){
      resp = *userver_loadfile("test.html");
      resp.mime = "text/html" USERVER_HEADER_LINE;
      return resp;
    }
    if(strcmp(loc, "/favicon") == 0 || strcmp(loc, "/favicon.ico") == 0){
      resp.payload =
        "<svg version=\"1.1\" width=\"256\" height=\"256\" xmlns=\"http://www.w3.org/2000/svg\">"
          "<circle cx=\"128\" cy=\"128\" r=\"64\" fill=\"#888\" />"
        "</svg>";
      resp.mime = "image/svg+xml" USERVER_HEADER_LINE;
      return resp;
    }
    if(strcmp(loc, "/main.c") == 0 || strcmp(loc, "/main.txt") == 0){
      resp = *userver_loadfile("main.c");
      resp.mime = "text/plain" USERVER_HEADER_LINE;
      return resp;
    }
    if(strcmp(loc, "/readme.md") == 0 || strcmp(loc, "/readme.txt") == 0){
      resp = *userver_loadfile("readme.md");
      resp.mime = "text/plain" USERVER_HEADER_LINE;
      return resp;
    }
    if(strcmp(loc, "/userver.h") == 0 || strcmp(loc, "/userver.txt") == 0){
      resp = *userver_loadfile("userver.h");
      resp.mime = "text/plain" USERVER_HEADER_LINE;
      return resp;
    }
    resp.payload = "<h1>Cannot find resource</h1>";
  }else{
    resp.payload = "<h1>Request not implemented</h1>";
  }
  return resp;
}

/**
 * main()
 *
 * Entry point into our program. Parse the command line parameters and start
 * the server as required.
 *
 * @param argc The number of command line arguments.
 * @param argv The command line arguments.
 **/
int main(int argc, char** argv){
  /* Setup default arguments */
  int port = 9999;
  char* dir = "./";
  /* Loop over arguments */
  for(int x = 0; x < argc; x++){
    /* Check we have an argument */
    if(argv[x][0] == '-'){
      /* No arguments */
      switch(argv[x][1]){
        case 'h' :
          fprintf(stderr,
            "./userver [OPT]\n"
            "  OPTions\n"
            "    -d  Set a directory to server <STR>\n"
            "    -h  Display this help\n"
            "    -p  Set a port to bind to <INT>\n"
          );
          return 0;
      }
      /* One argument */
      if(x + 1 < argc){
        switch(argv[x][1]){
          case 'd' :
            dir = argv[++x];
            break;
          case 'p' :
            port = atoi(argv[++x]);
            break;
        }
      }
    }else{
      fprintf(stderr, "Unknown argument '%s'\n", argv[x]);
    }
  }
  /* Setup the server */
  /* NOTE: Server RAM ~= file size * cache size, which here is ~16MB. */
  if(userver_init(port, dir, 512, 1 << 14, 1 << 10) != 0){
    fprintf(stderr, "Failed to start userver\n");
    return -1;
  }
  /* Call the main server loop */
  userver_loop(1000);
}
